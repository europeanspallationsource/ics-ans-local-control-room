#!/bin/bash

ANSIBLE_VERSION="2.3.1.0"

if [[ "$(whoami)" != "root" ]]
then
  echo "ERROR! $0 shall be run as root."
  exit 1
fi

# Install ansible-playbook and ansible-galaxy PEX files
for exe in ansible-playbook ansible-galaxy
do
  echo "Installing $exe"
  curl --fail -o /usr/local/bin/${exe} https://artifactory.esss.lu.se/artifactory/swi-pkg/ansible-releases/${ANSIBLE_VERSION}/${exe}
  chmod a+x /usr/local/bin/${exe}
done

echo "Creating ansible.cfg"
mkdir -p /etc/ansible
cat <<EOF > /etc/ansible/ansible.cfg
[defaults]
inventory      = /etc/ansible/hosts

# logging is off by default unless this path is defined
# if so defined, consider logrotate
log_path = /var/log/ansible.log
EOF

echo "Creating Ansible inventory"
cat <<EOF > /etc/ansible/hosts
[local-control-room]
localhost ansible_connection=local
EOF

# Configure logrotate
cat << EOF > /etc/logrotate.d/ansible
/var/log/ansible.log {
    missingok
    notifempty
    size 100k
    monthly
    create 0666 root root
}
EOF

echo "Cloning ics-ans-local-control-room"
yum install -y git
rm -rf /root/ics-ans-local-control-room
git clone https://bitbucket.org/europeanspallationsource/ics-ans-local-control-room /root/ics-ans-local-control-room

mkdir -p /etc/ansible/group_vars
cp /root/ics-ans-local-control-room/group_vars/local-control-room /etc/ansible/group_vars/
cp /root/ics-ans-local-control-room/playbook.yml /etc/ansible/

echo "Downloading Ansible roles"
/usr/local/bin/ansible-galaxy install -r /root/ics-ans-local-control-room/requirements.yml --force -p /etc/ansible/roles/

echo "Running Ansible playbook"
/usr/local/bin/ansible-playbook /etc/ansible/playbook.yml
