ics-ans-local-control-room
==========================

Ansible playbook to install a local control room machine.


Usage
-----

The playbook can be run locally or from an Ansible server.

To run locally on the machine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Download the script `bootstrap-local.sh` and run it as root::

    $ cd /tmp
    $ curl -O https://bitbucket.org/europeanspallationsource/ics-ans-local-control-room/raw/master/bootstrap-local.sh
    $ chmod a+x /tmp/bootstrap-local.sh
    $ sudo /tmp/bootstrap-local.sh


To run from an Ansible server
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

You must have ansible >= 2.3.0.0 already installed.

::

    $ git clone https://bitbucket.org/europeanspallationsource/ics-ans-local-control-room.git
    $ cd ics-ans-local-control-room
    # Edit the "hosts" file
    $ make playbook


License
-------

BSD 2-clause
