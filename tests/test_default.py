import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    '.molecule/ansible_inventory').get_hosts('all')


def test_csstudio_version(File):
    version = File('/opt/cs-studio/ess-version.txt').content_string
    assert version.strip() == '4.5.0.0'
